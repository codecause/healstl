/* globals */
var root_dir = "/admin/";

/* content */

var contentFunctions = function() {

  // variables
  var $content = $(".content");
  
  var keyValuePairsHTML = $content.find(".key-value-pairs").html(),
      contentHTML = $content.html(),
      newContentCount = 0;

  // functions
  function updateContent(id) {
    var $thisContent = $("[data-id=" + id + "]"),
        isNewContent = id[0] == "n";

    var $updateButton = $thisContent.find(".update-content"),
        $deleteButton = $thisContent.find(".delete-content");

    var processing_text = isNewContent ? "Adding..." : "Updating...",
        complete_text   = isNewContent ? "Added!" : "Updated!";
    
    $updateButton.text(processing_text).prop("disabled", true);
    $deleteButton.prop("disabled", true)

    var $pairs = $thisContent.find(".key-value-pairs");
        options = {};

    $.each($pairs, function(i, pair) {
      var key   = $(pair).find("input.key").val(),
          value = $(pair).find("input.value").val()

      if (!key && !value) { $(pair).remove(); return true; }

      options[key] = value;
    });

    var post_data = {
      "slug": $thisContent.find("input").val(),
      "options": JSON.stringify(options)
    };
    
    if (!isNewContent) {
      post_data['id'] = id;
    }

    var post_url = isNewContent ? root_dir + "create/content" : root_dir + "update/content";

    $.post(post_url, post_data).done(function(data) {
      if (isNewContent) {
        var data = JSON.parse(data);
        $thisContent.attr("data-id", data['message']);
      }

      setTimeout(function() {
        $updateButton.text(complete_text);
        setTimeout(function() {
          $updateButton.text("Update").prop("disabled", false);
          $deleteButton.text("Delete").prop("disabled", false);
        }, 2000);
      }, 1000);
    });
  }

  function addContent() {
    newContentCount++;

    $(".add-content").next().after(''+
      '<div class="content hide" data-id="new' + newContentCount + '">' +
        contentHTML +
      '</div>');

    $thisContent = $(".content.hide");

    $thisContent.find(".key-value-pairs").remove();
    addKeyValuePair("new" + newContentCount);

    $thisContent.find("input").prop("value", "");
    $thisContent.find(".update-content").text("Add");
    $thisContent.find(".delete-content").text("Cancel");
    $thisContent.removeClass("hide");

    updateContentEventListener();
    deleteContentEventListener();
    addKeyValuePairEventListener();
    deleteKeyValuePairEventListener();
  }

  function deleteContent($el) {
    $el.parent().remove();
  }

  function addKeyValuePair(id) {
    $thisContent = $(".content").filter("[data-id='" + id + "']");

    $thisContent.find(".add.button").before('' +
      '<div class="row key-value-pairs hide">' +
        keyValuePairsHTML +
      '</div>');

    $thisKeyValue = $thisContent.find(".key-value-pairs.hide");

    $thisKeyValue.find("input").prop("value", "");
    $thisKeyValue.removeClass("hide");

    deleteKeyValuePairEventListener();
  }

  function deleteKeyValuePair($el) {
    $el.parent().parent().remove();
  }


  // events
  var updateContentEventListener = function() {
    $(".update-content").unbind().click(function() {
      var id = $(this).parent().attr("data-id");
      updateContent(id);
    });
  };

  var addContentEventListener = function() {
    $(".add-content").unbind().click(function() {
      addContent();
    });
  };

  var deleteContentEventListener = function() {
    $(".delete-content").unbind().click(function() {
      deleteContent($(this));
    });
  };

  var addKeyValuePairEventListener = function() {
    $(".content").find(".add").unbind().click(function() {
      var id = $(this).parent().attr("data-id");
      addKeyValuePair(id);
    });
  };

  var deleteKeyValuePairEventListener = function() {
    $(".content").find(".key-value-pairs").find(".delete").unbind().click(function() {
      deleteKeyValuePair($(this));
    });
  };


  // initiate listeners
  updateContentEventListener();
  addContentEventListener();
  deleteContentEventListener();
  addKeyValuePairEventListener();
  deleteKeyValuePairEventListener();
};



/* applications */
var applicationsFunctions = function() {

  // variables
  var $applications = $(".applications");


  // functions
  function updateApplication(id) {
    var $thisApplication = $("[data-id=" + id + "]");

    var $updateButton = $thisApplication.find(".update-application"),
        $deleteButton = $thisApplication.find(".delete-application");

    $updateButton.text("Updating...").prop("disabled", true);
    $deleteButton.prop("disabled", true);

    var post_data = {
      "id": id,
      "status": $thisApplication.find("select").find("option:selected").val()
    };

    $.post(root_dir + "update/applications", post_data).done(function(data) {
      setTimeout(function() {
        $updateButton.text("Updated!");
        setTimeout(function() {
          $updateButton.text("Update").prop("disabled", false);
          $deleteButton.text("Delete").prop("disabled", false);
        }, 2000);
      }, 1000);
    });
  }

  function deleteApplication(id) {
    var $thisApplication = $("[data-id=" + id + "]");

    $.post(root_dir + "delete/applications", {"id": id}).done(function(data) {
      $thisApplication.remove();
    });
  }


  // events
  var updateApplicationEventListener = function() {
    $(".update-application").click(function() {
      var id = $(this).parent().attr("data-id");
      updateApplication(id);
    });
  };

  var deleteApplicationEventListener = function() {
    $(".applications").find(".delete-application").click(function() {
      var id = $(this).parent().attr("data-id");
      deleteApplication(id);
    });
  };

  // initiate listeners
  updateApplicationEventListener();
  deleteApplicationEventListener();
};



/* users */
var usersFunctions = function() {

  // variables
  var $users = $(".users");
  var userHTML = $users.html(),
      userCount = 0;

  // functions
  function updateUser(id) {
    var $thisUser = $("[data-id=" + id + "]"),
        isNewUser = id[0] == "n";

    var $updateButton = $thisUser.find(".update-user"),
        $deleteButton = $thisUser.find(".delete-user");

    var processing_text = isNewUser ? "Adding..." : "Updating...",
        complete_text   = isNewUser ? "Added!" : "Updated!";
     
    $updateButton.text(processing_text).prop("disabled", true);
    $deleteButton.prop("disabled", true)

    var post_data = {
      "email": $thisUser.find("input[type='text']").val()
    };

    var password = $thisUser.find("input[type='password']").val();

    if (password) {
      post_data['password'] = password;
    }

    if (!isNewUser) {
      post_data['id'] = id;
    }

    var post_url = isNewUser ? root_dir + "create/users" : root_dir + "update/users";
    
    $.post(post_url, post_data).done(function(data) {

      if (isNewUser) {
        var data = JSON.parse(data);
        $thisUser.attr("data-id", data['message']);
      }

      setTimeout(function() {
        $updateButton.text(complete_text);
        setTimeout(function() {
          $thisUser.find("input[type='password']").val("");
          $updateButton.text("Update").prop("disabled", false);
          $deleteButton.text("Delete").prop("disabled", false);
        }, 2000);
      }, 1000);

    });
  }

  function addUser() {
    userCount++;

    $(".add-user").next().after('' +
      '<div class="users hide" data-id="new' + userCount + '">' +
      userHTML +
      '</div>');

    $thisUser = $(".users.hide");

    $thisUser.find("input").prop("value", "");
    $thisUser.find(".update-user").text("Add");
    $thisUser.find(".delete-user").text("Cancel");
    $thisUser.removeClass("hide");

    updateUserEventListener();
    deleteUserEventListener();
  }

  function deleteUser(id) {
    var $thisUser = $("[data-id=" + id + "]"),
        isNewUser = id[0] == "n";

    if (isNewUser) {
      $thisUser.remove();
      return false;
    }

    $.post(root_dir + "delete/users", {"id": id}).done(function(data) {
      $thisUser.remove();
    });
  }

  // events
  var updateUserEventListener = function() {
    $(".update-user").click(function() {
      var id = $(this).parent().attr("data-id");
      updateUser(id);
    });
  };

  var addUserEventListener = function() {
    $(".add-user").click(function() {
      addUser();
    });
  };

  var deleteUserEventListener = function() {
    $(".delete-user").click(function() {
      var id = $(this).parent().attr("data-id");
      deleteUser(id);
    });
  }


  // initiate listeners
  updateUserEventListener();
  addUserEventListener();
  deleteUserEventListener();
};


var loginFunctions = function() {

  $("form").submit(function(e){
    e.preventDefault();

    var post_data = {
      email: $('input[name="email"]').val(),
      password: $('input[name="password"]').val()
    };

    $.post('/admin/login', post_data).done(function(data) {
      window.location.replace('/admin/content');
    }).fail(function(data) {
      console.log(JSON.parse(data));
    });

  });

};