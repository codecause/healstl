<?php

function getAllApplications() {
  return queryDatabase("SELECT * FROM applications ORDER BY id DESC");
}

function createApplication($options) {
  include('users.php');
  $subject = 'New Submission on CodeCause.org at ' . date("m-d-y g:i.s");
  $message = 'Check out the new form that has been submitted on CodeCause.org at codecause.org/admin/applications';
  
  foreach (getAllUsers() as $user) {
    mail($user['email'], $subject, $message);
  }

  $date = date("m-d-y");
  return queryDatabase("INSERT INTO applications (options, status, date_submitted, ip) VALUES (?, ?, ?, ?)", array($options, "Received", $date, getClientIP()));
}

function updateApplicationStatus($id, $status) {
  return queryDatabase("UPDATE applications SET status = ? WHERE id = ?", array($status, $id));
}

function deleteApplication($id) {
  return queryDatabase("DELETE FROM applications WHERE id = ?", array($id));
}

?>