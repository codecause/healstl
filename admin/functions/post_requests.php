<?php
  
$url    = array_reverse($url);
$target = $url[0]; // ie account
$action = $url[1]; // ie 'update'

$error  = "Target of " . $target . " does not have an of action " . $action . ".";

function create($target) {
  switch ($target) {
    case 'content':
      createContent($_POST['slug'], $_POST['options']);
      break;
    case 'users':
      createUser($_POST['email'], $_POST['password']);
      break;
    default:
      createResponse("error", $error);
  }
  createResponse("success", $target . " successfully created");
}

function update($target) {
  switch ($target) {
    case 'content':
      updateContent($_POST['id'], $_POST['slug'], $_POST['options']);
      break;
    case 'applications':
      updateApplicationStatus($_POST['id'], $_POST['status']);
      break;
    case 'users':
      updateUser($_POST['id'], $_POST['email'], $_POST['password']);
      break;
    default:
      createResponse("error", $error);
  }
  createResponse("success", $target . " successfully updated");
}

function delete($target) {
  switch ($target) {
    case 'content':
      deleteContent($_POST['id']);
      break;
    case 'applications':
      deleteApplication($_POST['id']);
      break;
    case 'users':
      deleteUser($_POST['id']);
      break;
    default:
      createResponse("error", $error);
  }
  createResponse("success", $target . " successfully deleted");
}

function submit($target) {
  switch ($target) {
    case 'applications':
      createApplication($_POST['fields']);
      break;
    default:
      createResponse("error", $error);
  }
}

function createResponse($status, $message) {
  $response = array("status"  => $status);
  if ($status == "error") { http_response_code(401); }
  if ($message) { $response['message'] = $message; }

  exit(json_encode($response));
}

if ($target == "login") { tryLogin($_POST['email'], $_POST['password']) ? createResponse("sucess") : createResponse("error", 'Not valid login'); }

// mini-router
switch ($action) {
  case "create":
    create($target);
    break;
  case "update":
    update($target);
    break;
  case "delete":
    delete($target);
    break;
  case "submit":
    submit($target);
    break;
  default:
    createResponse("error", $error);
};

?>