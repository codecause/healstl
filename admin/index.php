<?php

session_start();

// mini-router
$url = explode('/', trim($_SERVER['REQUEST_URI'], '/'));
$slug = array_reverse($url)[0];

if ($slug == 'admin') { header('location:content'); }
if ($slug == 'logout') { session_destroy(); header('location:/'); }
$file = 'pages/' . $slug . '.php';

include('functions/functions.php');

if (!$_SESSION['email'] && $slug !== 'login') { header('location:/admin/login'); }


$_SERVER['REQUEST_METHOD'] == 'POST' ? include('functions/post_requests.php') : include('pages/page.php');

?>