﻿<?php include('functions.php'); ?>
<!doctype html>
<html class="no-js" lang="en">
	
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		<title>#HealSTL | Donate</title>

		<link rel="stylesheet" href="css/foundation.css" />
		<link rel="stylesheet" href="css/app.css" />
		<link href='http://fonts.googleapis.com/css?family=Paytone+One|Muli:300,400|Open+Sans:300' rel='stylesheet' type='text/css'>

		<script src="js/vendor/modernizr.js"></script>
        <script>
            (function (p, s, h) {
                (p[s] = p[s] || []).push(
                    ["_setAccount", "543825c4572b94d733283030"],
                    ["_setCDN", h],
                    ["_setUrl", "https://assets.pushup.com"],
                    ["_displayBar"]
                );

                s = (p = p.document).createElement("script");
                s.src = h + "/pushup.min.js";
                p.getElementsByTagName("head")[0].appendChild(s)
            })(window, "_pa", "https://cdn.pushup.com");
        </script>
	</head>
  
  	<body>
		<div class="contain-to-grid sticky">	
			<nav class="top-bar" data-topbar>
				<ul class="title-area">
					<li class="name">	
						<div class="row">
							<div class="large-12 medium-12 small-6 small-centered columns">
								<h1 class="logo"><a href="index.php"><img class="size" src="img/logo.png" alt="#healSTL" /></a></h1>
							</div>	
						</div>
					</li>
					<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
					<li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
				</ul>
			
				<section class="top-bar-section">
					<!-- Right Nav Section -->
					<ul class="right">
						<li><a href="volunteer.php">Volunteer</a></li>
						<li><a href="outreach.php">Outreach</a></li>
						<li><a href="about.php">About</a></li>
						<li><a href="https://squareup.com/market/healstl/healstl-t-shirt">T-Shirts</a></li>
						<li><a href="donate.php" class="button tiny" >Donate</a></li>
					</ul>
				</section>
			</nav>
		</div>
  	
  		<div class="parallex donate">
  			<div class="row">
  				<div class="large-12 medium-12 columns">
  					<h1><?php getContent('donate', 'title'); ?></h1>		
  				</div>
  			</div>
  		</div>	
  		
  		<!--donation content below-->
  		<div class="row">
  			<div class="small-12 medium-6 large-6 columns">
  			    <h2><?php getContent('donate_info', 'title1'); ?></h2>
  				<p><?php getContent('donate_info', 'text1'); ?></p>	
             	<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
             	        <input type="hidden" name="cmd" value="_s-xclick">
             	        <input type="hidden" name="hosted_button_id" value="9FTGEVYTPCV5L">
             	        <input type="submit" class="button tiny text-centered" name="submit" value="Donate" alt="PayPal - The safer, easier way to pay online!">
             	        <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
             	    </form>
            </div>
			<div class="small-12 medium-6 large-6 columns">
			    <h2><?php getContent('donate_info', 'title2'); ?></h2>
				<p><?php getContent('donate_info', 'text2'); ?></p>	
				<a href="<?php getContent('donate_info', 'link'); ?>" class="button tiny"><?php getContent('donate_info', 'button_name'); ?></a> 	
			</div>
  		</div>
 
	  <!--footer-->
	  	<div class="footer">
	  		<div class="row">
	  			  	<div class="large-12 columns">
	  				<p>Powered by <a target="_blank" href="http://www.codecause.org">CodeCause.org</a></p>
	  			</div>
	  		</div>
	  	</div>
	       
	       
	  <!--foundation js-->     
	    <script src="js/vendor/jquery.js"></script>
	    <script src="js/foundation.min.js"></script>
	    <script>
	      $(document).foundation();
	    </script>
 	 
 	 </body>

</html>
