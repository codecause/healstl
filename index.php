﻿<?php include('functions.php'); ?>

<!doctype html>
<html class="no-js" lang="en">
	<head>
    	<meta charset="utf-8" />
    	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    	
    	<title>#HealSTL</title>
    	
    	<link rel="stylesheet" href="css/foundation.css" />
    	<link rel="stylesheet" href="css/app.css" />
    	<link rel="stylesheet" href="css/flex.css" />
    	<link href='http://fonts.googleapis.com/css?family=Paytone+One|Muli:300,400|Open+Sans:300' rel='stylesheet' type='text/css'>
	
   		<script src="js/vendor/modernizr.js"></script>
  	</head>
  	<body>
  		<div class="contain-to-grid sticky">	
  			<nav class="top-bar" data-topbar>
    			<ul class="title-area">
					<li class="name">	
						<div class="row">
							<div class="large-12 medium-12 small-6 small-centered columns">
								<h1 class="logo"><a href="index.php"><img class="size" src="img/logo.png" alt="#healSTL" /></a></h1>
							</div>	
						</div>
					</li>
					
					<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
					<li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
			    </ul>
  
    			<section class="top-bar-section">
      			<!-- Right Nav Section -->
				
					<ul class="right">
					  <li><a href="volunteer.php">Volunteer</a></li>
					  <li><a href="outreach.php">Outreach</a></li>
					  <li><a href="about.php">About</a></li>
					  <li><a href="https://squareup.com/market/healstl/healstl-t-shirt">T-Shirts</a></li>
					  <li><a href="donate.php" class="button tiny" >Donate</a></li>
					</ul>
  				</section>
  			</nav>
  		</div>
  	
  		<div class="parallex home">
  			<div class="row">
  				<div class="large-12 medium-12 columns">
  					<h1 class="long"><?php getContent('home', 'title'); ?></h1>
  				</div>
  			</div>
  		</div>	
  		
  		<div class="row lift">
  			<div class=" small-11 small-centered  large-10 large-centered columns">
  				<dl class="tabs" data-tab>
  			  		<dd class="active"><a href="#panel1"><?php getContent('home_panel_1', 'tab'); ?></a></dd>
  			  		<dd><a href="#panel2"><?php getContent('home_panel_2', 'tab'); ?></a></dd>
  			  		<dd><a href="#panel3"><?php getContent('home_panel_3', 'tab'); ?></a></dd>
  				</dl>
  			</div>
  		</div>	
  		
  		<div class="tabs-content">
  			  <div class="content active" id="panel1">
  			  	<div class="row">
  			  		<div class="large-12 columns">
  			  			<h2><?php getContent('home_panel_1', 'title'); ?></h2>
  			  		</div>
  			  	</div>
  			  	<!--mail Chimp goes here-->
  			  	<div class="row">
  			    	<div class="small-12 medium-6 large-6 columns">
  			    		<h3><?php getContent('home_panel_1', 'text'); ?> 
  			    		</h3>
  			    	</div>
  			    	<div class="small-12 medium-6 large-6 columns">
  			    	<div id="mc_embed_signup">
                        <form action="//21stward.us1.list-manage.com/subscribe/post?u=f8374d18f9ca34ba013b9aacf&amp;id=2ce377f49a" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                            <div id="mc_embed_signup_scroll">
                                <p class="title">Subscribe to our Mailing List</p>
                                <div class="mc-field-group">
                                    <label for="mce-EMAIL">Email Address  
                                        <span class="asterisk">*</span>
                                    </label>
                                        <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                                </div>
                                <div class="mc-field-group">
                                    <label for="mce-FNAME">First Name </label>
                                    <input type="text" value="" name="FNAME" class="" id="mce-FNAME">
                                </div>
                        <div class="mc-field-group">
                            <label for="mce-LNAME">Last Name </label>
                            <input type="text" value="" name="LNAME" class="" id="mce-LNAME">
                        </div>
                        <div id="mce-responses" class="clear">
                            <div class="response" id="mce-error-response" style="display:none"></div>
                            <div class="response" id="mce-success-response" style="display:none"></div>
                        </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div style="position: absolute; left: -5000px;"><input type="text" name="b_f8374d18f9ca34ba013b9aacf_2ce377f49a" tabindex="-1" value=""></div>
                                <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                            </div>
                        </form>
                    </div>
<!--End mc_embed_signup-->
  			    	</div>
  			    </div>
  			  </div>
  			  <div class="content" id="panel2">
  			  
  			  <div class="row">
  			  	<div class="large-12 columns">
  			  		<h2><?php getContent('home_panel_2', 'title'); ?></h2>
  			  	</div>
  			  </div>
  			  
  			  	<div class="row">
  			  		<div class="small-12 medium-6 large-6 columns">
  			  			<p><?php getContent('home_panel_2', 'text'); ?>
  			  			</p>
  			  		</div>
  			  		<div class="small-12 medium-6 large-6 columns">
  			  			<form method="post" action="">
  			  				<input type="text" name="firstname" placeholder="First Name" />
  			  				<input type="text" name="lastname" placeholder="Last Name" />
  			  				<input type="text" name="email" placeholder="Email" />
  			  				<input type="text" name="phone" placeholder="Phone Number" />
  			  				<label>Best time to be contacted:</label>
  			  				<input type="radio" name="time" value="morning" /> <label for="morning">Morning</label> <br />
  			  				<input type="radio" name="time" value="afternoon" /> <label for="afternoon">Afternoon</label> <br />
  			  				<input type="radio" name="time" value="evening" /> <label for="morning">Night</label> <br />
  			  				<input type="submit" name="" value="Submit" class="button tiny" />
  			  			</form>
  			  		</div>
  			  	</div> 
  			  </div>
  			  <div class="content" id="panel3">
  			  
	  			  <div class="row">
	  			  	<div class="large-12 columns">
	  			  		<h2><?php getContent('home_panel_3', 'title'); ?></h2>
	  			  	</div>
	  			  </div>
	  			  <div class="row">
	  			    <div class="large-12 columns">
	  			        <h3><?php getContent('home_panel_3', 'text'); ?></h3>
	  			    </div>
	  			  </div>
	  			  <div class="row">
	  			    <div class="small-6 medium-6 large-6 columns text-center">
	  			      <a target="_blank" href="https://www.sos.mo.gov/votemissouri/request.php" class="button small">Online Registration</a>
	  			    </div>
	  			    <div class="small-6 medium-6 large-6 columns text-center">
	  			      <a target="_blank" href="https://www.sos.mo.gov/elections/register2vote/St.Louis.pdf" class="button small">Download Registration</a>
	  			    </div>
	  			  </div>
  			  </div>
  			  </div>
  			  <!--used to locate community section on this page-->
  			  <div id="community">
  			  
  			  <div class="row">
  			  	<div class="large-12 columns">
  			  		<h2>Our Community</h2>
  			  	</div>
  			  </div>
  			  
  			  <div class="row">
                    <div class="small-12 medium-6 large-6 columns text-center">                      
                      <p class="title">@HealSTL</p>
                      <a class="twitter-timeline" href="https://twitter.com/HealSTL" data-widget-id="510583759662354432">Tweets by @HealSTL</a>
                      <script>!function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + "://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } }(document, "script", "twitter-wjs");
                      </script>
                    </div>

                      <div class="small-12 medium-6 large-6 columns text-center">
                      <p class="title">@AntonioFrench</p>
                      <a class="twitter-timeline" href="https://twitter.com/AntonioFrench" data-widget-id="381788274508853249">Tweets by @AntonioFrench</a>
                      <script>!function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + "://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } }(document, "script", "twitter-wjs");
                      </script>
                     </div>
            </div>
            </div>
  <!--footer-->
  	<div class="footer">
  		<div class="row">
  			  	<div class="large-12 columns">
  				<p>Powered by <a target="_blank" href="http://www.codecause.org">CodeCause.org</a></p>
  			</div>
  		</div>
  	</div>	 
       
       
  <!--foundation js-->     
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
    
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-55321242-1', 'auto');
        ga('send', 'pageview');

    </script>
   
    
  </body>
</html>
