<!-- Other css selectors are embedded in html elements, mostly just used for centering text, so should be easy to move into css file-->



.footer {
     font-size: 10px; 
     text-align: center;
}

h2 {
  font-family: 'Paytone One';
  font-size: 42px;
}
  
p {
  font-family: 'Muli';
  font-size: 16px;
}
